﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Lose : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision) 
    {
        // * Game Over Scene
        SceneManager.LoadScene("GameOverScene");
    }
}
