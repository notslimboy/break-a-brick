/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameStatus : MonoBehaviour
{

    [Range(0.1f, 10f)] [SerializeField] float gameSpeed = 1f;
    [SerializeField] int PointsPerBlockDestroyed = 10;
    [SerializeField] int currentScore = 0;
    [SerializeField] TextMeshProUGUI scoreText;


    private void Awake()
    {
        int gameStatusCount = FindObjectsOfType<GameStatus>().Length;
        if (gameStatusCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        Time.timeScale = gameSpeed;
    }

    private void Start()
    {
        scoreText.text = currentScore.ToString();
    }

    public void AddScore()
    {
        currentScore += PointsPerBlockDestroyed;
        scoreText.text = currentScore.ToString();
    }

    public void ResetGame()
    {
        Destroy(gameObject);
    }
}