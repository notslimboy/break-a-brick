﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    [SerializeField] float screenWidthUnits = 16f;
    [SerializeField] float minX = 1f;
    [SerializeField] float maxX = 15f;


    // Update is called once per frame
    void Update()
    {
        // * mouse position
        float mousePosUnits = Input.mousePosition.x / Screen.width * screenWidthUnits;
        // * paddle position 
        Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);
        paddlePos.x = Mathf.Clamp(mousePosUnits, minX, maxX);
        transform.position = paddlePos; // * reference to paddle Vector2 paddlePos
    }
}
