﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    // * Reference to Paddle
    [SerializeField] Paddle paddle;
    [SerializeField] float upDirection = 2f; // * direction that ball will launch refer to X 
    [SerializeField] float upForce = 15f; // * force power realese the ball refer to Y 
    // * State
    Vector2 paddleToBall;

    // * bool to check start of the game 
    bool hasStarted = false;

    // * Start is called before the first frame update
    void Start()
    {
        paddleToBall = transform.position - paddle.transform.position;
    }

    // * Update is called once per frame
    void Update()
    {
        if (!hasStarted)
        {
            LockBallToPaddle();
            LaunchOnMouseClick();
        }
    }

    private void LaunchOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            hasStarted = true;
            GetComponent<Rigidbody2D>().velocity = new Vector2(upDirection, upForce);
        }
    }

    private void LockBallToPaddle()
    {
        Vector2 paddlePos = new Vector2(paddle.transform.position.x, paddle.transform.position.y);
        transform.position = paddlePos + paddleToBall;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (hasStarted)
        {
            BallSoundTrigger();
        }
    }
    private void BallSoundTrigger()
    {
        GetComponent<AudioSource>().Play();
    }
}
