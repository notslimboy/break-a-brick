﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
public class Level : MonoBehaviour
{
    SceneLoader sceneLoader;
    [SerializeField] int breakableBlocks;
    public void CountBreakableBlocks()
    {
        breakableBlocks++;
    }

    private void Start()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
    } 

    public void BlockDestroyed()
    {
        breakableBlocks--;
        if(breakableBlocks <= 0)
        {
            sceneLoader.LoadNextScene();
        }
    }
}
