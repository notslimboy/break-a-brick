﻿/*
 *   Copyright (c) 2019 NotSlimBoy
 *   All rights reserved.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{

    [SerializeField] GameObject blockSparklesVFX;

    // * Cached Reference
    Level level;


    private void Start()
    {
        level = FindObjectOfType<Level>();
        level.CountBreakableBlocks(); // * count how much block in each scene
    }

    private void OnCollisionEnter2D(Collision2D collsion)
    {
        DestroyBlocks();
    }

    private void DestroyBlocks()
    {
        FindObjectOfType<GameStatus>().AddScore();
        Destroy(gameObject);
        TriggerSparklesVFX();
        level.BlockDestroyed();
    }


    private void TriggerSparklesVFX()
    {
        GameObject sparkles = Instantiate(blockSparklesVFX,transform.position,transform.rotation);
        Destroy(sparkles,1f); // *  destroy instantiate 
    }
}

